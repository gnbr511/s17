console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:
function getUserInfo(){
	let fullName = prompt('What is your name?');
	let age = prompt('How old are you?');
	let address = prompt('Where do you live?');
	alert("Thank you for your input!");
	console.log("Hello, " + fullName);
	console.log("You are " + age + "years old.");
	console.log("You live in " + address + ".");
};
getUserInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayMyTopFiveMusicalArtist(){
		console.log("My top 5 musical artist");
		console.log("1. Calum Scott");
		console.log("2. Sam Smith");
		console.log("3. Adele");
		console.log("4. Bruno Mars");
		console.log("5. Guy Sebastian")
	}
	displayMyTopFiveMusicalArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayMyTop5MoviesRating(){
		console.log("Top 5 Movies Rotten Tomatoes Rating")
		console.log("1. The Woman King");
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("2. Barbarian");
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("3. Pearl");
		console.log("Rotten Tomatoes Rating: 87%");
		console.log("4. Pinocchio");
		console.log("Rotten Tomatoes Rating: 27%");
		console.log("5. See How They Run");
		console.log("Rotten Tomatoes Rating: 72%");

	}
	displayMyTop5MoviesRating();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends()
// console.log(friend1);
// console.log(friend2);